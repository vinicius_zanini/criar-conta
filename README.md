<h1>Criando a sua conta</h1>

### Primeiro, deve-se acessar o site do GitLab: https://about.gitlab.com/ e clicar em "Get free trial" no campo superior direito

![Imagem1](/uploads/3de10be366dcb29b3c4cd2892f1fe40c/Imagem1.png)

### após isso, cria-se a conta na pagina direcionada e será pedido para você confirmar em seu email:

![Imagem2](/uploads/d551f4bfc85df5e0dbb5a4a1737da3cb/Imagem2.png)

### Confirmando a criação da conta, será pedido para você determinar sua função e o motivo de estar utilizando o GitLab. Podem preencher com qualquer uma das opções como mostrado no exemplo abaixo:

![Imagem3](/uploads/aa3905fe19a22c0a046b0d7cc06e7ccf/Imagem3.png)

### Por fim, você pode escolher entre colocar os dados a mais que são pedidos ou simplesmente pular o teste da conta "ultimate", clicando no botão "Skip Trial" em azul.

![Imagem4](/uploads/7e9bf587c76464df3ebcb1354346d6fb/Imagem4.png)
